#!/usr/bin/env python3

"""Script for printing various percentiles from data such as flow sizes or
flow interarrival times.
"""

import argparse


def parse_args():
    parser = argparse.ArgumentParser(description='Print the percentiles ' \
            'data such as flow sizes or flow interarrival times')

    parser.add_argument('file', help='path to a file that contains a list of ' \
            'values such as flow sizes or flow interarrival times (one ' \
            'number per line)')

    return parser.parse_args()


def get_sorted_list_of_flow_sizes(flow_size_file):
    with open(flow_size_file) as f:
        lines = f.readlines()
        flow_sizes = [int(line) for line in lines if line != '']
        return sorted(flow_sizes)


def get_percentile(list_length, p):
    return int(round(list_length * p))


def print_percentiles(flow_sizes):
    l = len(flow_sizes)

    p25 = get_percentile(l, 0.25)
    p50 = get_percentile(l, 0.5)
    p75 = get_percentile(l, 0.75)
    p90 = get_percentile(l, 0.9)
    p95 = get_percentile(l, 0.95)
    p97_5 = get_percentile(l, 0.975)
    p99 = get_percentile(l, 0.99)
    p99_9 = get_percentile(l, 0.999)

    p2_5 = get_percentile(l, 0.025)
    p5 = get_percentile(l, 0.05)
    p10 = get_percentile(l, 0.1)

    print('p25:', flow_sizes[p25])
    print('p50:', flow_sizes[p50])
    print('p75:', flow_sizes[p75])
    print('p90:', flow_sizes[p90])
    print('p95:', flow_sizes[p95])
    print('p97.5:', flow_sizes[p97_5])
    print('p99:', flow_sizes[p99])
    print('p99.9:', flow_sizes[p99_9])
    print()

    print('p2.5 -- p97.5:', flow_sizes[p2_5], '--', flow_sizes[p97_5])
    print('p5 -- p95:', flow_sizes[p5], '--', flow_sizes[p95])
    print('p10 -- p90:', flow_sizes[p10], '--', flow_sizes[p90])


def main():
    args = parse_args()
    input_file = args.file

    flow_sizes = get_sorted_list_of_flow_sizes(input_file)

    print_percentiles(flow_sizes)


if __name__ == '__main__':
    main()

