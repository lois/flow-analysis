#!/usr/bin/env python3

"""Script for parsing a flow metrics JSON file and obtaining the flow sizes
out of it.
"""

import argparse
import os
import sys
import ijson


def parse_args():
    """Parse command line arguments and return the args namespace.
    """

    parser = argparse.ArgumentParser(description='Parse a flow metrics JSON ' \
            'file and print the flow sizes of all flows in the JSON.')

    parser.add_argument('json', help='a json file containing the flow ' \
            'analysis results, or a directory, in which case this script ' \
            'will recurse throug all sub-directories and parse every json ' \
            'in any sub-directory')
    parser.add_argument('-c', '--client', action='store_true',
                        help='use {size,packets}Client not Server (needed ' \
                                'e.g. for unidirectional CAIDA datasets)')
    parser.add_argument('-p', '--packets', action='store_true',
                        help='get flow sizes in packets; omitting this ' \
                                'option (default) get flow sizes in bytes')
    parser.add_argument('-u', '--udp', action='store_true',
                        help='output UDP flow sizes; omitting this option ' \
                                '(default) outputs TCP flow sizes')

    return parser.parse_args()


def parse_json(
        json_file: str,
        packets: bool = False,
        udp: bool = False,
        client: bool = False,
        ):
    """Parse a flow metrics JSON file and print the flow sizes.
    This JSON is one as produced by the scalable flow analyzer ("analysis"
    binary).

    Args:
        json_file: the path to the flow metrics JSON file
        packets: if True, get flow sizes in packets, otherwise in bytes
        udp: if True, parse UDP flow sizes, otherwise parse TCP flow sizes
        client: when True, parse size of the client not the server
    """

    with open(json_file) as f:
        for k, v in ijson.kvitems(f, ''):
            protocol = 'UDP' if udp else 'TCP'
            if v['protocol'] != protocol:
                continue

            bytes_counter = 'sizeClient' if client else 'sizeServer'
            packets_counter = 'packetsClient' if client else 'packetsServer'
            size_counter = packets_counter if packets else bytes_counter
            
            size = v[size_counter]

            if v[packets_counter] == 1:
                continue

            if size == 0:
                continue

            print(size)


def recurse_directory(
        directory: str,
        packets: bool = False,
        udp: bool = False,
        client: bool = False,
        ):
    """Recursively walk through a directory and parse all flow metrics JSON
    files in any subdirectory.
    This function parses every file in any subdirectory of the specified
    directory. It recognizes a JSON simply by the file ending '.json'.

    Args:
        directory: the directory to recurse
        packets: if True, get flow sizes in packets, otherwise in bytes
        udp: if True, parse UDP flow sizes, otherwise parse TCP flow sizes
        client: when True, parse size of the client not the server
    """

    for root, dirs, files in os.walk(directory):
        for f in files:
            if f.endswith('.json'):
                parse_json(f, packets, udp)


def main():
    args = parse_args()
    json_path = args.json
    client = args.client
    packets = args.packets
    udp = args.udp

    if not (os.path.isfile(json_path) or os.path.isdir(json_path)):
        print(json_path, 'does not exist!')
        sys.exit(1)

    if os.path.isdir(json_path):
        recurse_directory(json_path, packets, udp, client)
    else:
        parse_json(json_path, packets, udp, client)


if __name__ == '__main__':
    main()

