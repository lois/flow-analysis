#!/usr/bin/env python3

"""Script for converting time values in a flow interarrival times file from
nanoseconds into microseconds.
"""

import argparse


def parse_args():
    parser = argparse.ArgumentParser(description='Convert the values in a ' \
            'flow interarrival times file from nanoseconds to microseconds. ' \
            'This script reads a file and prints the converted values to ' \
            'stdout.')
    parser.add_argument('file', help='path to a file containing flow ' \
            'interarrival times as created by get_flow_interarrival_times.py')
    return parser.parse_args()


def main():
    args = parse_args()
    ns_file = args.file
    with open(ns_file) as f:
        lines = f.readlines()
        for line in lines:
            ns = int(line)
            us = ns // 1000
            print(us)


if __name__ == '__main__':
    main()

