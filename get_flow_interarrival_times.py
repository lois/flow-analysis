#!/usr/bin/env python3

"""Script for parsing a flow metrics JSON file and obtaining the flow
interarrival times out of it.
"""

import argparse
import os
import sys
import ijson


def parse_args():
    """Parse command line arguments and return the args namespace.
    """
    parser = argparse.ArgumentParser(description='Parse a flow metrics JSON ' \
            'file and print the flow interarrival times of all flows in the ' \
            'JSON.')
    parser.add_argument('json', help='a json file containing the flow ' \
            'analysis results, or a directory, in which case this script ' \
            'will recurse through all sub-directories and parse every json ' \
            'in any sub-directory')
    parser.add_argument('-u', '--udp', action='store_true',
                        help='output UDP flow inter-arrival times; omitting ' \
                                'this option (default) outputs TCP ' \
                                'inter-arrival times')
    return parser.parse_args()


def get_flow_start_times_from_json(
        json_file: str,
        udp: bool = False,
        ) -> list:
    """Parse a flow metrics JSON file and return a sorted list of flow
    start times. This JSON is one as produced by the scalable flow analyzer
    ("analysis" binary).

    Args:
        json_file: the path to the flow metrics JSON file
        udp: if True, parse UDP flow sizes, otherwise parse TCP flow sizes

    Returns:
        a sorted list of integers that contains all flow start times as 64
          bit Unix timestamps in ns
    """

    flow_start_times = []

    with open(json_file) as f:
        try:
            for k, v in ijson.kvitems(f, ''):
                protocol = 'UDP' if udp else 'TCP'
                if v['protocol'] != protocol:
                    continue

                flow_start = v['start']
                flow_start_times.append(flow_start)
        except ijson.common.IncompleteJSONError as e:
            # This error can happen at the end of a CAIDA JSON
            # It is safe to ignore it in our case; the flow_start_times list
            # will still be filled correctly
            pass

    if len(flow_start_times) <= 1:
        print('Not enough flows in the JSON!')
        sys.exit(1)

    return sorted(flow_start_times)


def get_flow_interarrival_times(
        json_file: str,
        udp: bool = False,
        ) -> list:
    """Obtain the flow interarrival times from a flow metrics JSON file and
    return a sorted list of interarrival times.
    This JSON is one as produced by the scalable flow analyzer ("analysis"
    binary).

    Args:
        json_file: the path to the flow metrics JSON file
        udp: if True, parse UDP flow interarrival times, otherwise parse
          TCP flow interarrival times

    Returns:
        a sorted list of flow interarrival times in ns
    """

    flow_start_times = get_flow_start_times_from_json(json_file, udp)

    flow_interarrival_times = []

    for i in range(len(flow_start_times) - 1):
        cur_start_time = flow_start_times[i]
        next_start_time = flow_start_times[i+1]
        interarrival_time = next_start_time - cur_start_time
        flow_interarrival_times.append(interarrival_time)

    return sorted(flow_interarrival_times)


def recurse_directory(
        directory: str,
        udp: bool = False,
        ):
    """Recursively walk through a directory and parse all flow metrics JSON
    files in any subdirectory.
    This function parses every file in any subdirectory of the specified
    directory. It recognizes a JSON simply by the file ending '.json'.

    Args:
        directory: the directory to recurse
        udp: if True, parse UDP flow sizes, otherwise parse TCP flow sizes
    """

    all_interarrival_times = []

    for root, dirs, files in os.walk(directory):
        for f in files:
            if f.endswith('.json'):
                current_interarrival_times = get_flow_interarrival_times(f, udp)
                all_interarrival_times.append(current_interarrival_times)

    for t in sorted(all_interarrival_times):
        print(t)


def main():
    args = parse_args()
    json_path = args.json
    udp = args.udp

    if not (os.path.isfile(json_path) or os.path.isdir(json_path)):
        print(json_path, 'does not exist!')
        sys.exit(1)

    if os.path.isdir(json_path):
        recurse_directory(json_path, udp)
    else:
        interarrival_times = get_flow_interarrival_times(json_path, udp)
        for t in interarrival_times:
            print(t)


if __name__ == '__main__':
    main()

