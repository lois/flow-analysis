#!/usr/bin/env python3

"""Script for plotting flow sizes or flow interarrival times as CDF or PDF.
"""

import argparse
import os
import sys
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import scipy.stats
from collections import Counter

# PAPER FONT SIZE #
plt.rcParams.update({'font.size': 18})

# Formatting for paper #
matplotlib.rcParams['pdf.fonttype'] = 42
matplotlib.rcParams['ps.fonttype'] = 42


LINESTYLES = [
        'solid',
        (0, (1, 1)),
        (0, (3, 1)),
        (0, (5, 1)),
        (0, (5, 5)),
        (0, (3, 1, 1, 1)),
        (0, (3, 1, 1, 1, 1, 1)),
        (0, (1, 1, 3, 1, 3, 1, 1, 1)),
]


def parse_args():
    """Parse command line arguments.
    """

    parser = argparse.ArgumentParser(description='Draw for one or multiple ' \
            'lists of flow sizes or flow interarrival times a PDF and a ' \
            'CDF.\nIf you provide one file as input, the plots are stored ' \
            'into the directory in which your input file is located. If you ' \
            'provide multiple files as input, the plots are stored into the ' \
            'directory in which you run the script.')

    parser.add_argument('file', nargs='+', help='path to one or more files ' \
            'that contain a list of flow sizes or flow interarrival times ' \
            '(one number per line)')
    parser.add_argument('-c', '--cleanup-tail', '--cleanup-outliers',
                        action='store_true', help='remove all values that ' \
                                'lie beyond the 95th percentile to remove ' \
                                'the extreme tail of the data')
    parser.add_argument('-o', '--output', help='the prefix for the names of ' \
            'the output files (default: flow_size, i.e. the output files ' \
            'will be flow_size_cdf.pdf etc.)')
    parser.add_argument('-l', '--labels', nargs='+',
                        help='labels for the files you provide')
    parser.add_argument('-x', '--xlabel', help='the xlabel to use for the ' \
            'plot (default: "Flow size")')
    parser.add_argument('-L', '--linear', action='store_true',
                        help='use linear axis scale (default: log scale)')

    return parser.parse_args()


def get_sorted_lists_of_values(
        flow_size_files: list,
        cleanup_tail: bool = False,
        ) -> list:
    """Parse a file that contains the values (flow sizes or flow
    interarrival times) and return a sorted list of those values.

    Args:
        flow_size_files: a list of files that contain on each line a flow
          size
        cleanup_tail: if True, remove flow size values that lie beyond the
          95th percentile to remove the extreme tail of the data

    Returns:
        a list of sorted lists of values (list of list of int); for each
          input file, there is one list of values
    """

    flow_size_lists = []
    for flow_size_file in flow_size_files:
        with open(flow_size_file) as f:
            lines = f.readlines()
            values = [int(line) for line in lines if line != '']
            sorted_flow_size_list = sorted(values)

            if cleanup_tail:
                p95 = int(round(len(values) * 0.95))
                sorted_flow_size_list = sorted_flow_size_list[:p95]
            
            flow_size_lists.append(sorted_flow_size_list)

    return flow_size_lists


def plot_cdf(
        values_per_file: list,
        labels: list,
        xlabel: str,
        output_path: str,
        output_prefix: str = 'flow_size',
        linear: bool = False,
        ):
    """Plot a CDF of the values.

    Args:
        values_per_file: a list of sorted lists of values; for each input
          file, there is one list of values
        labels: a list of strings defining the labels for the input files.
          Will be shown in a legend; if labels is None, there will be no
          legend
        xlabel: the xlabel to use in the plot
        output_path: the path in which to store the plot pdf; if None, show
          the plot directly
        output_prefix: the prefix of the output file name
        linear: whether to use linear scale or not (default: False, so use
          log scale)
    """

    fig, ax = plt.subplots()

    if not linear:
        ax.set_xscale('log')

    ax.set_xlabel(xlabel)
    ax.set_ylabel('CDF')

    ax.grid(axis='y', alpha=0.5)

    for i, values in enumerate(values_per_file):
        label = None if labels is None else labels[i]
        ax.step(values, np.linspace(0, 1, len(values)), label=label,
                linestyle=LINESTYLES[i % len(LINESTYLES)])

    if labels:
        ax.legend(loc='lower right')

    fig.tight_layout()

    if output_path is None:
        fig.show()
    else:
        fig.savefig(os.path.join(output_path, output_prefix + '_cdf.pdf'))


def get_axes_for_pdf(
        values: list,
        normalize: bool = True,
        ) -> tuple:
    """Get the values for x and y axes for a PDF from the flow sizes.

    On the x axis, put the values of the flow sizes that occur in the data.
    On the y axis, put how often each value ocurs.

    Args:
        values: a sorted list of flow sizes (list of int)
        normalize: whether to normalize the y-axis; if True, the y-axis
          displays how often a value occurs as a percentage (i.e.
          count/len(values)), otherwise as absolute count

    Returns:
        a tuple(list[int], list[int]) that contains the values for each axis
          as list (i.e., one list of flow size values that occur, and one
          list of how often they occur)
    """

    x = []  # flow size or flow interarrival time values
    y = []  # counts of these values
    previous_value = -1  # init with -1 such that the first
                             # current_value != previous_value

    for current_value in values:
        if current_value != previous_value:
            x.append(current_value)
            y.append(1)
        else:
            y[-1] = y[-1] + 1
        previous_value = current_value

    if normalize:
        y = [y_val / len(values) for y_val in y]

    return x, y


def plot_pdf(
        values_per_file: list,
        labels: list,
        xlabel: str,
        output_path: str,
        output_prefix: str = 'flow_size',
        linear: bool = False,
        ):
    """Plot a PDF of the values.

    Args are the same as in plot_cdf().
    """

    fig, ax = plt.subplots()

    if not linear:
        ax.set_xscale('log')

    ax.set_xlabel(xlabel)
    ax.set_ylabel('PDF')

    ax.grid(axis='y', alpha=0.5)

    for i, values in enumerate(values_per_file):
        x, y = get_axes_for_pdf(values)
        label = None if labels is None else labels[i]
        ax.plot(x, y, label=label, linestyle=LINESTYLES[i % len(LINESTYLES)])

    if labels:
        ax.legend()

    fig.tight_layout()

    if output_path is None:
        fig.show()
    else:
        fig.savefig(os.path.join(output_path, output_prefix + '_pdf.pdf'))


def main():
    args = parse_args()
    flow_size_files = args.file
    cleanup_tail = True if args.cleanup_tail else False
    output_prefix = args.output if args.output else 'flow_size'
    labels = args.labels if args.labels else None
    xlabel = args.xlabel if args.xlabel else 'Flow size'
    linear = True if args.linear else False

    values_per_file = get_sorted_lists_of_values(flow_size_files,
                                                 cleanup_tail=cleanup_tail)
    output_dir = os.path.dirname(flow_size_files[0]) \
            if len(flow_size_files) == 1 else os.getcwd()

    plot_cdf(values_per_file, labels, xlabel, output_dir, output_prefix, linear)
    plot_pdf(values_per_file, labels, xlabel, output_dir, output_prefix, linear)


if __name__ == '__main__':
    main()

