#!/usr/bin/env python3

"""This script converts all *pcap.gz files in a directory to .pcapng.gz.
CAIDA traces, for example, need to be converted.
"""

import argparse
import os
import subprocess


def convert_file(path: str, filename: str):
    pcapgz = os.path.join(path, filename)
    subprocess.run(['gzip', '-d', pcapgz])

    filename_without_gz = filename[:-3]
    pcap = os.path.join(path, filename_without_gz)
    filename_pcapng = filename_without_gz + 'ng'
    pcapng = os.path.join(path, filename_pcapng)
    subprocess.run(['editcap', pcap, pcapng])

    os.remove(pcap)

    subprocess.run(['gzip', pcapng])

    print('Converted ' + filename + ' to ' + filename_pcapng + '.gz')


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('path', help='path to the directory that contains the pcap.gz files')
    args = parser.parse_args()
    path = args.path

    for filename in os.listdir(path):
        if filename.endswith('.pcap.gz'):
            convert_file(path, filename)


if __name__ == '__main__':
    main()
