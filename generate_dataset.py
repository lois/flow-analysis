#!/usr/bin/env python3

"""Script for generating a dataset for flow sizes or flow interarrival
times. The dataset can be cutoff at certain percentiles or at specific
values.
"""

import argparse
import sys


def parse_args():
    parser = argparse.ArgumentParser(description='Generate a dataset that ' \
            'can be used by the evaluation framework from a file containing ' \
            'flow sizes or flow interarrival times.\n' \
            'This script takes all values and prints them sorted.\n' \
            'If you specify one percentile, it only prints values up to ' \
            'that percentile. If you specify two percentiles, it prints all ' \
            'values between the first and second percentile.\n' \
            'If you specify one limit, this script only prints values less ' \
            'than or equal to the limit. If you specify two limits, it ' \
            'prints values between the two limits.')

    parser.add_argument('file', help='path to the input file from which you ' \
            'want to generate the dataset')
    parser.add_argument('-p', '--percentile', nargs='+',
                        help='specify one or two percentiles to limit the ' \
                                'range of values')
    parser.add_argument('-l', '--limit', nargs='+',
                        help='specifiy one or two limits to limit the range ' \
                                'of values')

    return parser.parse_args()


def get_sorted_list_of_input_data(flow_size_file: str) -> list:
    with open(flow_size_file) as f:
        lines = f.readlines()
        input_data = [int(line) for line in lines if line != '']
        return sorted(input_data)


def parse_percentiles(percentiles: list) -> tuple:
    if len(percentiles) == 1:
        p_lower = 0
        p_upper = float(percentiles[0])
    elif len(percentiles) == 2:
        p_lower = float(percentiles[0])
        p_upper = float(percentiles[1])
    else:
        print('Illegal number of percentiles:', len(percentiles))
        sys.exit(1)

    if p_lower >= 1:  # input like "1"
        p_lower = p_lower / 100
    if p_upper >= 1:  # input like "95"
        p_upper = p_upper / 100
    # else, input is like 0.01 or 0.99, which is the correct form

    return p_lower, p_upper


def print_dataset(
        input_data: list,
        percentiles: list,
        limits: list,
        ):
    if percentiles:
        p_lower, p_upper = parse_percentiles(percentiles)

        cutoff_lower = int(round(len(input_data) * p_lower))
        cutoff_upper = int(round(len(input_data) * p_upper))
        input_data = input_data[cutoff_lower:cutoff_upper]

    for val in input_data:
        if limits:
            if ( (len(limits) == 1 and val > int(limits[0]))
                    or (len(limits) == 2 and
                    (val < int(limits[0]) or val > int(limits[1]))) ):
                continue

        print(val)


def main():
    args = parse_args()
    input_file = args.file
    percentiles = args.percentile
    limits = args.limit

    input_data = get_sorted_list_of_input_data(input_file)

    print_dataset(input_data, percentiles, limits)


if __name__ == '__main__':
    main()

