# Flow Analysis
This repository contains a set of scripts for analyzing flows from packet
traces. In particular, it contains useful scripts for analyzing results from
the Scalable Flow Analyzer
(<https://github.com/uncatchable-de/scalable-flow-analyzer>) that can be used
for generating flows with the CC Slowdown Evaluation Framework
(<https://gitlab.tudelft.nl/lois/cc-fct-eval-framework>).

## Overview
Here is a brief summary of the contents of this repository and what each script
does:
* `get_flow_sizes.py`: Parse a flow metrics JSON file (as created by the
scalable flow analyzer) for flow sizes and print all flow sizes found in the
JSON
* `get_flow_interarrival_times.py`: Parse a flow metrics JSON file (as created
by the scalable flow analyzer) for flow inter-arrival times and print all flow
inter-arrival times for the flows in the JSON
* `generate_dataset.py`: Cut off percentiles or limit the values in a dataset
that contains flow sizes or flow inter-arrival times; useful for removing the
extreme tail of your data
* `plot_distribution_functions.py`: Draw a PDF and CDF for a dataset that
contains flow sizes or flow inter-arrival times to visualize your data
* `print_percentiles.py`: Calculate and print various percentiles of a dataset
that contains flow sizes or flow inter-arrival times
* `convert_pcaps.py`: Mass-convert all pcap.gz files in a directory to
pcapng.gz
* `convert_flow_interarrival_times_from_ns_to_us.py`: Convert time values in a
flow inter-arrival times dataset from nanoseconds to microseconds and print the
converted dataset
* `datasets.tar.gz`: Some datasets created using traces by MAWI
(<https://mawi.wide.ad.jp/mawi/>), CAIDA
(<https://www.caida.org/catalog/datasets/passive_dataset/>), and a campus trace
from TU Delft
* `percentiles.txt`: a summary of the percentiles for the datasets from MAWI,
CAIDA, and the campus trace

## How to Generate a Dataset for the CC Slowdown Evaluation Framework
If you have a pcap or pcapng file and you want to create flow size and flow
interarrival time datasets out of it that you can use as input for the CC
Slowdown Evaluation Framework
(<https://gitlab.tudelft.nl/lois/cc-fct-eval-framework>),
this is how you do it:

0. If your file is pcap not pcapng: convert it using
`editcap input_file.pcap output_file.pcapng`;
if your trace is split into multiple pcap or pcapng files: merge them using
`mergecap -a part1.pcapng part2.pcapng -w output_file.pcapng` (mergecap should
also be able to handle pcap)
1. Use Scalable Flow Analyzer
(<https://github.com/uncatchable-de/scalable-flow-analyzer>) with your pcapng
file; this tool produces a flow metrics JSON file that contains a summary about
the flows in your pcapng
2. Use `get_flow_sizes.py` and/or `get_flow_interarrival_times.py` from this
repository to parse the flow sizes or flow inter-arrival times from the flow
metrics JSON; pipe the output into text files. You can use these text files as
input for the CC Slowdown Eval. Framework
3. Optional: Modify your data by cutting off a percentile or setting an upper
limit for values using `generate_dataset.py` from this repository
4. Optional: Flow inter-arrival times might be in nanoseconds, which is
typically a higher resolution than needed; convert the values in a flow
inter-arrival times dataset to microseconds using
`convert_flow_interarrival_times_from_ns_to_us.py` from this repostitory
